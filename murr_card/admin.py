from django.contrib import admin
from django_enum_choices.admin import EnumChoiceListFilter

from .models import MurrCard


@admin.register(MurrCard)
class MurrCardAdmin(admin.ModelAdmin):
    date_hierarchy = 'timestamp'
    list_filter = [('status', EnumChoiceListFilter)]
    search_fields = ['title', 'owner__username']
    ordering = ('-rating',)
    list_display = ('title', 'rating', 'owner')
