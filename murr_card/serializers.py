from django_enum_choices.serializers import EnumChoiceModelSerializerMixin
from rest_framework import serializers
from django.conf import settings

from .models import MurrCard, EditorImageForMurrCard


class MurrCardSerializers(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    owner_name = serializers.ReadOnlyField(source='owner.username')
    owner_avatar = serializers.SerializerMethodField()  # magic - trigger get_owner_avatar()
    comments_count = serializers.ReadOnlyField()

    class Meta:
        model = MurrCard
        fields = ('id',
                  'title',
                  'cover',
                  'content',
                  'rating',
                  'comments_count',
                  'owner',
                  'owner_name',
                  'owner_avatar',
                  'status')
        read_only_fields = ('rating',)

    def get_owner_avatar(self, obj):
        if obj.owner.murren_avatar and hasattr(obj.owner.murren_avatar, 'url'):
            return f'{settings.BACKEND_URL}{obj.owner.murren_avatar.url}'


class EditorImageForMurrCardSerializers(serializers.ModelSerializer):
    class Meta:
        model = EditorImageForMurrCard
        fields = ('murr_editor_image',)


class AllMurrSerializer(serializers.ModelSerializer):
    owner_name = serializers.ReadOnlyField(source='owner.username')
    owner_avatar = serializers.SerializerMethodField()
    likes = serializers.ReadOnlyField()
    dislikes = serializers.ReadOnlyField()
    comments_count = serializers.ReadOnlyField()

    class Meta:
        model = MurrCard
        fields = ('id',
                  'title',
                  'cover',
                  'rating',
                  'timestamp',
                  'owner',
                  'owner_name',
                  'owner_avatar',
                  'likes',
                  'dislikes',
                  'comments_count')
        read_only_fields = ('rating', 'timestamp')

    def get_owner_avatar(self, obj):
        if obj.owner.murren_avatar and hasattr(obj.owner.murren_avatar, 'url'):
            return f'{settings.BACKEND_URL}{obj.owner.murren_avatar.url}'
