import json
import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from murr_card.models import MurrCard, MurrCardStatus


@pytest.mark.django_db
def test_murr_card_create(create_murren):
    assert MurrCard.objects.count() == 0

    murren = create_murren

    data = {
        'title': 'TestMurrCard',
        'content': 'It is TestCard',
        'owner': murren.username,
    }
    json_data = json.dumps(data)

    api = APIClient()
    api.force_authenticate(user=murren)

    url = reverse('murr_card-list')
    response = api.post(url, data=json_data, content_type='application/json')
    assert response.status_code == status.HTTP_201_CREATED
    assert MurrCard.objects.count() == 1
    assert murren == MurrCard.objects.last().owner


@pytest.mark.django_db
def test_murr_card_delete(create_murren):
    murren = create_murren
    murr1 = MurrCard.objects.create(
        title='TestMurrCard1', content='It is TestMurrCard1', owner=murren, status=MurrCardStatus.RELEASE)
    murr2 = MurrCard.objects.create(
        title='TestMurrCard2', content='It is TestMurrCard2', owner=murren, status=MurrCardStatus.RELEASE)

    assert MurrCard.objects.all().count() == 2

    api = APIClient()
    api.force_authenticate(user=murren)

    data = {
        'title': murr1.title,
        'content': murr1.content,
        'owner': murren.username,
        'status': 'release',
    }

    json_data = json.dumps(data)

    url = reverse('murr_card-detail', args=(murr1.pk,))
    response = api.delete(url, data=json_data, content_type='application/json')
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert MurrCard.objects.all().count() == 1
