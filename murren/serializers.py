from rest_framework import serializers

from .models import Murren


class MurrenSerializer(serializers.ModelSerializer):

    class Meta:
        model = Murren
        fields = ('id', 'username', 'email', 'murren_avatar', 'murren_url')
        read_only_fields = ('murren_url',)
