from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from django.contrib.auth import get_user_model
from rest_auth.registration.views import SocialLoginView
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from murren.serializers import MurrenSerializer
from .permissions import IsAuthenticatedAndMurrenOrReadOnly

Murren = get_user_model()


class MurrenViewSet(ModelViewSet):
    queryset = Murren.objects.filter(is_active=True).order_by('-date_joined')
    serializer_class = MurrenSerializer
    permission_classes = [IsAuthenticatedAndMurrenOrReadOnly]

    @action(detail=False, permission_classes=[IsAuthenticated])
    def profile(self, request):
        murren = MurrenSerializer(instance=request.user)
        return Response(murren.data)


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter
